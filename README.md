devuan-sanity
=============

A collection of various system tweaks that can be installed on a Devuan
system to improve quality of life.


devuan-sanity-systemctl
-----------------------

Written by `bgstack15`, this package introduces a `systemctl` translator
script that turns `systemctl` acalls into `service` and `update-rc.d`
commands.
